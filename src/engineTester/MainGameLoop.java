package engineTester;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector3f;

import entities.Camera;
import entities.Entity;
import entities.Light;
import entities.Player;
import models.RawModel;
import models.TexturedModel;
import renderEngine.MasterRenderer;
import renderEngine.display.DisplayManager;
import renderEngine.loaders.Loader;
import renderEngine.loaders.ModelData;
import renderEngine.loaders.OBJLoader;
import terrains.Terrain;
import textures.ModelTexture;
import textures.TerrainTexture;
import textures.TerrainTexturePack;

public class MainGameLoop {

	public static void main(String[] args) {

		DisplayManager.createDisplay();
		Loader loader = new Loader();

		// Load models
		ModelTexture treeTexture = new ModelTexture(loader.loadTexture("tree"));
		TexturedModel tree = new TexturedModel(loadObj("tree", loader), treeTexture);

		ModelTexture lowTreeTexture = new ModelTexture(loader.loadTexture("lowPolyTree"));
		TexturedModel lowTree = new TexturedModel(loadObj("lowPolyTree", loader), lowTreeTexture);

		ModelTexture grassTexture = new ModelTexture(loader.loadTexture("grassTexture"));
		TexturedModel grass = new TexturedModel(loadObj("grassModel", loader), grassTexture);
		grass.getTexture().setHasTransparency(true);
		grass.getTexture().setUseFakeLight(true);

		ModelTexture fernTexture = new ModelTexture(loader.loadTexture("fern"));
		TexturedModel fern = new TexturedModel(loadObj("fern", loader), fernTexture);
		fern.getTexture().setHasTransparency(true);

		ModelTexture flowerTexture = new ModelTexture(loader.loadTexture("flower"));
		TexturedModel flower = new TexturedModel(loadObj("grassModel", loader), flowerTexture);
		flower.getTexture().setHasTransparency(true);

		// Handle Terrain Generation
		List<Entity> entities = new ArrayList<Entity>();
		Random random = new Random();
		for (int i = 0; i < 1000; i++) {
			entities.add(
					new Entity(tree, new Vector3f(random.nextFloat() * 1600, 0, random.nextFloat() * 800), 0, 0, 0, 3));

			entities.add(new Entity(lowTree, new Vector3f(random.nextFloat() * 1600, 0, random.nextFloat() * 800), 0, 0,
					0, 0.3f));

			entities.add(new Entity(grass, new Vector3f(random.nextFloat() * 1600, 0, random.nextFloat() * 800), 0, 0,
					0, 1));

			entities.add(new Entity(fern, new Vector3f(random.nextFloat() * 1600, 0, random.nextFloat() * 800), 0, 0, 0,
					0.6f));

			if (random.nextInt(101) < 20)
				entities.add(new Entity(flower, new Vector3f(random.nextFloat() * 1600, 0, random.nextFloat() * 800), 0,
						0, 0, 1.5f));
		}

		Light light = new Light(new Vector3f(20000, 20000, 2000), new Vector3f(1, 1, 1));

		// Terrain Texture
		TerrainTexture backgroundTexture = new TerrainTexture(loader.loadTexture("grassy"));
		TerrainTexture rTexture = new TerrainTexture(loader.loadTexture("dirt"));
		TerrainTexture gTexture = new TerrainTexture(loader.loadTexture("pinkFlowers"));
		TerrainTexture bTexture = new TerrainTexture(loader.loadTexture("path"));

		TerrainTexturePack texturePack = new TerrainTexturePack(backgroundTexture, rTexture, gTexture, bTexture);
		TerrainTexture blendMap = new TerrainTexture(loader.loadTexture("blendMap"));

		Terrain terrain = new Terrain(0, 0, loader, texturePack, blendMap);
		Terrain terrain2 = new Terrain(1, 0, loader, texturePack, blendMap);
		
		MasterRenderer renderer = new MasterRenderer();
		
		ModelTexture playerTexture = new ModelTexture(loader.loadTexture("playerTexture"));
		TexturedModel player = new TexturedModel(loadObj("person", loader), playerTexture);
		Player playerEntity = new Player(player, new Vector3f(100, 0, 200), 0, 0, 0, 0.3f);
		Camera camera = new Camera(playerEntity);		
		// Render
		while (!Display.isCloseRequested()) {
			camera.move();

			playerEntity.move();
			renderer.processEntity(playerEntity);
			renderer.processTerrain(terrain);
			renderer.processTerrain(terrain2);
			for (Entity entity : entities) {
				renderer.processEntity(entity);
			}
			renderer.render(light, camera);
			DisplayManager.updateDisplay();
		}

		renderer.cleanUp();
		loader.cleanUp();
		DisplayManager.closeDisplay();
	}

	public static RawModel loadObj(String filename, Loader loader) {
		ModelData objData = OBJLoader.loadOBJ(filename);
		return loader.loadToVAO(objData.getVertices(), objData.getTextureCoords(), objData.getNormals(),
				objData.getIndices());
	}
}
