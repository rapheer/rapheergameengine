package textures;

public class TerrainTexture {

	private int textureId;

	public TerrainTexture(int textureId) {
		this.textureId = textureId;
	}

	public int getID() {
		return textureId;
	}
}
