package entities;

import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector3f;

public class Camera {

	private static final float MINIMUM_DISTANCE_FROM_PLAYER = 15f;
	private static final float MAXIMUM_DISTANCE_FROM_PLAYER = 60f;
	private static final float MINIMUM_CAMERA_PITCH = 15f;
	private static final float MAXIMUM_CAMERA_PITCH = 60f;

	private float distanceFromPlayer = MINIMUM_DISTANCE_FROM_PLAYER;
	private float angleAroundPlayer = 0;

	private Player player;
	private Vector3f position = new Vector3f();
	private float pitch = MINIMUM_CAMERA_PITCH;
	private float yaw;
	private float roll;
	private float yOffset = 3;

	public Camera(Player player) {
		this.player = player;
	}

	public void move() {
		calculateZoom();
		calculatePitch();
		calculateAngleAroundPlayer();
		float horizontalDistance = calculateHorizontalDistance();
		float verticalDistance = calculateVerticalDistance();
		calculateCameraPosition(horizontalDistance, verticalDistance);
		this.yaw = 180 - (player.getRotY() + angleAroundPlayer);
	}

	public Vector3f getPosition() {
		return position;
	}

	public float getPitch() {
		return pitch;
	}

	public float getYaw() {
		return yaw;
	}

	public float getRoll() {
		return roll;
	}

	private void calculateCameraPosition(float horizontalDistance, float verticalDistance) {
		float theta = player.getRotY() + angleAroundPlayer;
		float xOffset = (float) (horizontalDistance * Math.sin(Math.toRadians(theta)));
		float zOffset = (float) (horizontalDistance * Math.cos(Math.toRadians(theta)));
		position.x = player.getPosition().x - xOffset;
		position.z = player.getPosition().z - zOffset;
		position.y = (player.getPosition().y + verticalDistance) + yOffset;

	}

	private float calculateHorizontalDistance() {
		return (float) (distanceFromPlayer * Math.cos(Math.toRadians(pitch)));
	}

	private float calculateVerticalDistance() {
		return (float) (distanceFromPlayer * Math.sin(Math.toRadians(pitch)));
	}

	private void calculateZoom() {
		float zoomLevel = Mouse.getDWheel() * 0.1f;
		distanceFromPlayer -= zoomLevel;
		if (distanceFromPlayer < MINIMUM_DISTANCE_FROM_PLAYER)
			distanceFromPlayer = MINIMUM_DISTANCE_FROM_PLAYER;
		else if (distanceFromPlayer > MAXIMUM_DISTANCE_FROM_PLAYER)
			distanceFromPlayer = MAXIMUM_DISTANCE_FROM_PLAYER;
	}

	private void calculatePitch() {
		if (Mouse.isButtonDown(1)) {
			float pitchChange = Mouse.getDY() * 0.1f;
			pitch -= pitchChange;
			if (pitch < MINIMUM_CAMERA_PITCH)
				pitch = MINIMUM_CAMERA_PITCH;
			else if (pitch > MAXIMUM_CAMERA_PITCH)
				pitch = MAXIMUM_CAMERA_PITCH;
		}
	}

	private void calculateAngleAroundPlayer() {
		if (Mouse.isButtonDown(0)) {
			float angleChange = Mouse.getDX() * 0.3f;
			angleAroundPlayer -= angleChange;
		}
	}
}
